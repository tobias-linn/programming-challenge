package de.exxcellent.challenge;

import de.exxcellent.data.FileManager;
import de.exxcellent.processing.DataOperations;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The entry class for your solution. This class is only aimed as starting point and not intended as baseline for your software
 * design. Read: create your own classes and packages as appropriate.
 *
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public final class App {

    /**
     * This is the main entry method of your program.
     *
     * @param args The CLI arguments passed
     *             Should be: "path to data" "file type"
     */
    public static void main(String... args) {

        if (!App.verifyParams(args)) {
            System.err.println("Parameters should be \"path\" \"file type\"");
            System.exit(0);
        }

        // Weather data
        FileManager fileManager = new FileManager();
        HashMap<String, ArrayList<String>> weatherData = fileManager.getData(args[0] + File.separator + "weather." + args[1].toLowerCase(), args[1]);
        if (weatherData == null) {
            System.err.println("No data");
            System.exit(0);
        }
        DataOperations dataOperations = new DataOperations(weatherData);

        String dayWithSmallestTempSpread = dataOperations.getSmallestDifference("MxT", "MnT", "Day");
        System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);

        // Football data
        HashMap<String, ArrayList<String>> footballData = fileManager.getData(args[0] + File.separator + "football." + args[1].toLowerCase(), args[1]);
        if (footballData == null) {
            System.err.println("No data");
            System.exit(0);
        }
        dataOperations.replaceData(footballData);

        String teamWithSmallestGoalSpread = dataOperations.getSmallestDifference("Goals", "Goals Allowed", "Team");
        System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallestGoalSpread);
    }

    /**
     * Make sure we have at least three parameters.
     * @param args parameters
     * @return True if three or more parameters are present.
     */
    private static boolean verifyParams(String... args) {
        return args.length > 1;
    }
}
