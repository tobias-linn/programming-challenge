package de.exxcellent.data;

import java.util.*;
import java.util.stream.Stream;

/**
 * This class contains methods to read in csv files and return them in a default data structure.
 */
public class Csv {

    private final LinkedHashMap<String, ArrayList<String>> data = new LinkedHashMap<>();

    /**
     * Convert a given string into the appropriate format.
     * @param rawData Raw string data, e.g. from a file.
     * @return A HashMap that uses the values of the first line as keys and the columns, beginning at the second line, as data stored in an ArrayList.
     */
    public HashMap<String, ArrayList<String>> processCsvString(String rawData) throws Exception {
        if (rawData.isBlank()) {
            throw new Exception("Processing string is empty!");
        }

        Stream<String> lines = rawData.lines();
        Iterator<String> iter = lines.iterator();
        if (iter.hasNext()) {
            generateKeys(iter.next());
        }
        while (iter.hasNext()) {
            addData(iter.next());
        }

        return data;
    }

    /**
     * Clears the data map and adds all fields from the given comma separated string to the data map.
     * Usually this is the first line of a csv file containing the column names.
     * @param line The comma separated string.
     * @throws Exception An exception is thrown if the given string is empty.
     */
    private void generateKeys(String line) throws Exception {
        if (!this.data.isEmpty()) {
            this.data.clear();
        }

        for (String key : line.split(",")) {
            if (key.isBlank()) {
                throw new Exception("Header contains an empty field!");
            }
            this.data.put(key.trim(), new ArrayList<>());
        }
    }

    /**
     * Takes a string as input that should be a row of a csv file and adds the entries by column to the data map.
     * @param line The comma separated string.
     * @throws Exception An exception is thrown if the number of fields in th string is not equal to the number of keys in the data map.
     */
    private void addData(String line) throws Exception {
        String[] lineAsArray = line.split(",");

        if (lineAsArray.length != this.data.size()) {
            throw new Exception("Number of values (" + lineAsArray.length + " does not match number of keys (" + this.data.size() + "!");
        }

        Iterator<ArrayList<String>> iter = data.values().iterator();
        for (String value : line.split(",")) {
            if (iter.hasNext()) {
                iter.next().add(value.trim());
            }
        }
    }
}
