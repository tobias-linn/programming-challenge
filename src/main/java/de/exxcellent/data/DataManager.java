package de.exxcellent.data;

import java.util.ArrayList;
import java.util.HashMap;

public interface DataManager {
    /**
     * Read in a file.
     * @param path The path of the file to read given as string.
     * @param type The type of the file, e.g. csv, as string.
     * @return Return a HashMap where keys represent the data name and the value is a list of all data.
     */
    HashMap<String, ArrayList<String>> getData(String path, String type);
}
