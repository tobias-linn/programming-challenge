package de.exxcellent.data;

import de.exxcellent.constants.FileSupport;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;

public class FileManager implements DataManager {

    @Override
    public HashMap<String, ArrayList<String>> getData(String path, String type) {
        String rawData;
        Csv csv = new Csv();
        try {
            rawData = this.readFile(path, type);
            return csv.processCsvString(rawData);
        } catch (Exception e) {
            System.err.println("An error occurred while getting the data: " + e.getMessage());
        }
        return null;
    }

    /**
     * Read in a file.
     * @param path The path of the file to read given as string.
     * @param type The type of the file, e.g. csv, as string.
     * @return Return a String that contains the whole content of the file.
     */
    public String readFile(String path, String type) throws Exception {
        if (!this.isSupportedFileType(type)) {
            throw new Exception("File type not supported");
        }

        File file = new File(path);
        if (!file.exists()) {
            throw new FileNotFoundException("File \"" + path + "\" not Found!");
        }

        return new String(Files.readAllBytes(file.toPath()));
    }

    /**
     * Check if the given type is supported by checking the corresponding enum.
     * @param type Type to check
     * @return True if {@link de.exxcellent.constants.FileSupport} contains type.
     */
    private boolean isSupportedFileType(String type) {
        for (FileSupport fileSupport : FileSupport.values()) {
            if (fileSupport.name().equals(type.trim().toUpperCase())) {
                return true;
            }
        }
        return false;
    }
}
