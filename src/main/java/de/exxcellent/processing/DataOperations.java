package de.exxcellent.processing;

import de.exxcellent.utils.OtherMath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class DataOperations {

    private HashMap<String, ArrayList<String>> data;

    public DataOperations(HashMap<String, ArrayList<String>> data) {
        this.data = data;
    }

    /**
     * Get a value of the row with the lowest difference between two values of the same row.
     * @param data1 Column (key) of the minuend.
     * @param data2 Column (key) of the subtrahend.
     * @param dataToReturn Column (key) of the difference.
     * @return Returns the value of the difference column where the difference ist minimal.
     */
    public String getSmallestDifference(String data1, String data2, String dataToReturn) {
        if (data1.isBlank() || data2.isBlank() || dataToReturn.isBlank()) {
            System.err.println("Missing column!!");
            return "";
        }

        if (!this.data.containsKey(data1) || !this.data.containsKey(data2) || !this.data.containsKey(dataToReturn)) {
            System.err.println("Column not found!");
            return "";
        }

        ArrayList<String> column1 = this.data.get(data1);
        ArrayList<String> column2 = this.data.get(data2);

        if (column1.size() != column2.size()) {
            System.err.println("Columns have note the same length!");
            return "";
        }

        Iterator<String> iter1 = column1.iterator();
        Iterator<String> iter2 = column2.iterator();
        double[] results = new double[column1.size()];
        int i = 0;
        while (iter1.hasNext()) {
            int value1;
            int value2;
            try {
                value1 = Integer.parseInt(iter1.next());
                value2 = Integer.parseInt(iter2.next());
                results[i] = Math.abs(value1 - value2);
                i++;
            } catch (NumberFormatException e) {
                System.err.println("An error occurred while calculating: " + e.getMessage());
            }
        }
        return this.data.get(dataToReturn).get(OtherMath.argMin(results));
    }

    public void replaceData(HashMap<String, ArrayList<String>> data) {
        this.data = data;
    }
}
