package de.exxcellent.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CsvTest {

    @Test
    void processSimpleStrings() throws Exception {

        HashMap<String, ArrayList<String>> testMap1 = new LinkedHashMap<>();
        testMap1.put("a", new ArrayList<>());
        testMap1.get("a").add("1");
        testMap1.get("a").add("4");
        testMap1.put("b", new ArrayList<>());
        testMap1.get("b").add("2");
        testMap1.get("b").add("5");
        testMap1.put("c", new ArrayList<>());
        testMap1.get("c").add("3");
        testMap1.get("c").add("6");

        String testString1 = "a,b,c\n1,2,3\n4,5,6";

        Csv csv = new Csv();

        HashMap<String, ArrayList<String>> data = csv.processCsvString(testString1);
        assertEquals(testMap1, data);
        for (String key : data.keySet()) {
            assertEquals(testMap1.get(key), data.get(key));
        }

        String testString5 = "d,e,f\n2,3,4\n5,6,7";
        data = csv.processCsvString(testString5);
        assertNotEquals(testMap1, data);
        for (String key : data.keySet()) {
            assertNotEquals(testMap1.get(key), data.get(key));
        }
    }

    @Test
    void processIncorrectString() throws Exception {
        String emptyString = "";
        String testString2 = "a,b,c\n1,2,3,4\n5,6,7";
        String testString3 = "a,b,c\n1,2,3\n5,6";
        String testString4 = "a,,c\n1,2,3\n4,5,6";

        Csv csv = new Csv();
        assertThrows(Exception.class, () -> {csv.processCsvString(emptyString);});
        assertThrows(Exception.class, () -> {csv.processCsvString(testString2);});
        assertThrows(Exception.class, () -> {csv.processCsvString(testString3);});
        assertThrows(Exception.class, () -> {csv.processCsvString(testString4);});
    }
}