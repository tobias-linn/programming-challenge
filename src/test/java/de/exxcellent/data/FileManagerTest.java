package de.exxcellent.data;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.*;

class FileManagerTest {

    @Test
    void getData() throws Exception {
        HashMap<String, ArrayList<String>> testMap1 = new LinkedHashMap<>();
        testMap1.put("a", new ArrayList<>());
        testMap1.get("a").add("1");
        testMap1.get("a").add("4");
        testMap1.put("b", new ArrayList<>());
        testMap1.get("b").add("2");
        testMap1.get("b").add("5");
        testMap1.put("c", new ArrayList<>());
        testMap1.get("c").add("3");
        testMap1.get("c").add("6");

        File file = File.createTempFile("test", "csv");
        FileManager fileManager = new FileManager();
        String testString = "a,b,c\n1,2,3\n4,5,6";
        Files.writeString(file.toPath(), testString);

        HashMap<String, ArrayList<String>> data = fileManager.getData(file.getAbsolutePath(), "csv");
        assertEquals(testMap1, data);
        for (String key : data.keySet()) {
            assertEquals(testMap1.get(key), data.get(key));
        }
    }

    @Test
    void fileExists() throws Exception {
        File file = File.createTempFile("test", "csv");
        FileManager fileManager = new FileManager();
        String str = fileManager.readFile(file.getAbsolutePath(), "csv");
        assertEquals("", str);

        String someString = "some\nstring";
        Files.writeString(file.toPath(), someString);
        str = fileManager.readFile(file.getAbsolutePath(), "csv");
        assertEquals(someString, str);
    }

    @Test
    void wrongType() throws IOException {
        File file = File.createTempFile("test", "cs");
        FileManager fileManager = new FileManager();
        assertThrows(Exception.class, () -> {fileManager.readFile("", "cs");});
    }

    @Test
    void noFile() {
        FileManager fileManager = new FileManager();
        assertThrows(Exception.class, () -> {fileManager.readFile("", "csv");});
    }
}