package de.exxcellent.processing;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataOperationsTest {

    @Test
    void smallestDiff() {
        HashMap<String, ArrayList<String>> testMap1 = new LinkedHashMap<>();
        testMap1.put("a", new ArrayList<>());
        testMap1.get("a").add("1");
        testMap1.get("a").add("4");
        testMap1.put("b", new ArrayList<>());
        testMap1.get("b").add("1");
        testMap1.get("b").add("5");
        testMap1.put("c", new ArrayList<>());
        testMap1.get("c").add("x");
        testMap1.get("c").add("y");

        DataOperations dataOperations = new DataOperations(testMap1);

        String smallest = dataOperations.getSmallestDifference("a", "b", "c");
        assertEquals("x", smallest);
    }

    @Test
    void emptyAndWrongKeys() {
        HashMap<String, ArrayList<String>> testMap1 = new LinkedHashMap<>();
        testMap1.put("a", new ArrayList<>());
        testMap1.get("a").add("1");
        testMap1.get("a").add("4");
        testMap1.put("b", new ArrayList<>());
        testMap1.get("b").add("1");
        testMap1.get("b").add("5");
        testMap1.put("c", new ArrayList<>());
        testMap1.get("c").add("x");
        testMap1.get("c").add("y");

        DataOperations dataOperations = new DataOperations(testMap1);

        String str = dataOperations.getSmallestDifference("x", "b", "c");
        assertEquals("", str);
        str = dataOperations.getSmallestDifference("", "b", "c");
        assertEquals("", str);
    }
}